<!--#include file="MenuDb.asp"-->
<%

'=====================================================================
' 软件名称：AnyMenu 二级树状目录菜单系统
' 当前版本：Version UTF-8 1.0
' 版权声明：版权归卿羽科技所有（www.AnyKings.com）
' EMail：mocomor@qq.com
' 请保留以上版权声明信息

%>
<body height='100%' class="gradient" style="FILTER: progid:DXImageTransform.Microsoft.Gradient(startColorStr='#ddeeff', endColorStr='#FFFFFF', gradientType='1')"  >
<table width='100%' height='100%' border='0' cellPadding='2' cellSpacing='2' style="FILTER: progid:DXImageTransform.Microsoft.Gradient(startColorStr='#ddeeff', endColorStr='#FFFFFF', gradientType='1')" valign='top' style="BORDER-right: #0099cc 0px solid;" class="gradient">
  <tr>
   <td valign="top" >
   <div class="menu">
    <table width="99%"   border="0" cellpadding="0" cellspacing="0">
     <tr>
      <td height=25><a href="javascript: menu.openAll();"><img src="Icon/folder_modernist_up.png" border="0" align="absmiddle" alt="展开列表"></a> <a href="javascript: menu.closeAll();"><img src="Icon/folder_stuffed.png" border="0" align="absmiddle" alt="收缩列表"></a> |  <a href="Main.asp" target="FrmMain">使用说明</a> |
</td>
</tr>
     <tr>
      <td>
      <script type="text/javascript">
	  menu = new dTree('menu');
	  menu.config.target="FrmMain";
	  menu.add(0,-1,' 管理导航');
	  //--从数据库抽取父菜单fatherID=0
	  <%
sql="select * from Nav_Menu where fatherID=0 and isshow=1 order by paixu"
rs.open sql,conn,1,1
for r=1 to rs.recordcount
F_ID=Trim(rs("ID"))&79
%>
	  menu.add(<%=F_ID%>,0, '<%=Trim(rs("name"))%>','');
	  //--从数据库抽取子菜单fatherID=Trim(rs("ID"))
 <%
sqls="select * from Nav_Menu where fatherID="&Trim(rs("ID"))&" and isshow=1 order by paixu"
rss.open sqls,conn,1,1
for s=1 to rss.recordcount
%>
menu.add(<%=F_ID&s%>,<%=F_ID%>,'<%=Trim(rss("name"))%>','<%=trim(rss("URL"))%>','<%=trim(rss("Comment"))%>','<%=trim(rss("URLTarget"))%>');
<%
rss.moveNext
Next
rss.close
rs.moveNext
Next
rs.close
%>
	  document.write(menu);
	  </script>

	  </td>
     </tr>
    </table>
   </div>
   </td>
  </tr>
</table>
</body>
</html>